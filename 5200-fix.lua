
-- version_5200_fix_lua = '2.0.0'

-- process BlickNote data within http input request, before submitted to Web-GUI

function main()
    local txr, txb, txd;

    local topDir = '/app';
    package.path = package.path.. topDir.. '/link/mod_security/rules/Lua/?.lua';
    require ("functions")

    txr = m.getvar ("tx.HiTech_5200_reason",    {"none"});
    txb = m.getvar ("tx.HiTech_5200_body",      {"none"});
    txd = m.getvar ("tx.HiTech_5200_descScope", {"none"});


    -- Data from http input requests arrives urlEncoded:
    -- Content-Type: application/x-www-form-encoded
    --
    txr = m.getvar ("tx.HiTech_5200_reason",    {"urlDecodeUni"});  -- {"lowercase", "urlEncode"}
    txb = m.getvar ("tx.HiTech_5200_body",      {"urlDecodeUni"});
    txd = m.getvar ("tx.HiTech_5200_descScope", {"urlDecodeUni"});


    -- Hack: Use txb for all, else the process is too complicated.
    -- Eg: different encoding styles for the same test text "Hello World" :
    --     Lua: 5200.1.txr='Hello%2520World'
    --     Lua: 5200.1.txb='Hello+World'
    --     Lua: 5200.1.txd='Hello%2520World'
    --
    txr = txb;
    txd = txb;


    -- Strategy 1: the BlickNote is:
    --      -	b64Encoded within http input request, before submitted to Web-GUI
    --      -	b64Decoded within output response body, before sent on to user's browser
    --
    --	Strategy 2: the BlickNote is:
    --      -	htmlEncoded within http input request, before submitted to Web-GUI
    --      -	htmlDecoded within output response body, before sent on to user's browser

    --	Strategy 3: the BlickNote is
    --      -	JDB-escaped within http input request, before submitted to Web-GUI
    --      -	JDB-unescaped within output response body, before sent on to user's browser
    --  - Strategy 1 also requires strategy 3, else the text is no casually readable.
    --	- Strategy 2 might not require strategy 3, though it's more deterministic to do so.

--[[
    -- Strategy 3: "Just in case:" First remove any stray JDB-escape patterns.
--]]

    txr = string.gsub(txr, "A0#0#", "");
    txr = string.gsub(txr, "#0#@$", "");
    --
    txb = string.gsub(txb, "A0#0#", "");
    txb = string.gsub(txb, "#0#0$", "");
    --
    txd = string.gsub(txd, "A0#0#", "");
    txd = string.gsub(txd, "#0#0$", "");


--[[
    -- Strategy 1: b64Encode
--]]

    txr = b64enc (txr);
    txb = b64enc (txb);
    txd = b64enc (txd);


    --	Drop any trailing '=' (padding characters), to avoid Javascript confusion.
    --	Strictly speaking, this is "base-64 non-compliant", but usually accepted anyway
    --      because the missing bytes can be infered automatically.

    txr = txr:gsub ('=+$', '');
    txb = txbrgsub ('=+$', '');
    txd = txd:gsub ('=+$', '');


--[[
    --	Strategy 2: htmlEncode
    --	For some reason, spaces must be '+' plus signs, not &#32;
    --	But first, protect the native '+' plus signs.
    --	ToDo: fails to work, the string %%2B appears instead of '+' plus.
    --
    --	txr = string.gsub(txr, "\+", "%%2B");
    --	txb = string.gsub(txb, "\+", "%%2B");
    --	txd = string.gsub(txd, "\+", "%%2B");
    -- --
    --	m.log (1, "Lua: 5200.4.txr='".. txr..
    --	if txr ~= txb then m.log (1, "Lua: 5200.4.txb='".. txb.. "'") end
    --	if txr ~= txd then m.log (1, "Lua: 5200.4.txd='".. txd.. "'") end

    --	Now replaces spaces with '+' signs.
    txr = string.gsub(txr, " ", "+");
    txb = string.gsub(txb, " ", "+");
    txd = string.gsub(txd, " ", "+");
    --
    --	m.log (1, "Lua: 5200.5. txr=' " . . txr..
    --	if	txr	~=	txb	then m.log (1, "Lua: 5200.5.txb='".. txb.. "'")	end
    --	if	txr	~=	txd	then m.log (1, "Lua: 5200.5.txd='".. txd.. "'")	end

    --	Pre-fix: The sequence \E will foil the PCRE regex in rsub. Fix: \E -> /E
    txr = string.gsub(txr, "\\E", "/E");
    txb = string.gsub(txb, "\\E", "/E");
    txd = string.gsub(txd, "\\E", "/E");
    --
    --	m.log (1, "Lua: 5200.6. txr=' " . . txr..
    --	if txr ~= txb then m.log (1, "Lua: 5200.6.txb='".. txb.. "'") end
    --	if txr ~= txd then m.log (1, "Lua: 5200.6.txd='".. txd.. "'") end

    txr = htmlEncode (txr);
    txb = htmlEncode (txb);
    txd = htmlEncode (txd);

    -- m.log (1, "Lua: 5200.7.txr='".. txr.. "'");
    --	if txr ~= txb then m.log (1, "Lua:	5200.7.txb='"..	txb.. "'") end
    --	if txr ~= txd then m.log (1, "Lua:	5200.7.txd='"..	txd.. "'") end

    -- Now urlEncode the results, returning the format to Content-Type: text/html
    txr = urlEncode (txr);
    txb = urlEncode (txb);
    txd = urlEncode (txd);

    -- m.log (1, "Lua: 5200.9.txr='".. txr.. "'");
    --	 if txr ~= txb then m.log (1, "Lua: 5200.9.txb='".. txb.. "'") end
    --	 if txr ~= txd then m.log (1, "Lua: 5200.9.txd='".. txd.. ",") end
    --	Pre-fix: The sequence \E will foil the PCRE regex in rsub.
    --	ToDo: htmlEncode the backslash.
    --	This is a bit tricky, as the output is already urlEncoded:
    --      Original:	txr='31.+hi+x*+\E+yo+y*'	\E	... -> &bsol;E
    --      After urlEnc:	txr='31.%2Bhi%2Bx%2A%2B%5CE%2Byo%2By%2A'
    --      -> %5CE	31.	hi	x* &bsol;E yo y*
    --      After fix here: txr='31.%2Bhi%2Bx%2A%2B%5CE%2Byo%2By%2A'	->	%26;bsol%3BEE
    --	But this fails to work, when performed AFTER the urlEncode() step:

    txr = string.gsub(txr, "%%5CE", "%%26;bsol%%3BE");
    txb = string.gsub(txr, "%%5CE", "%%26;bsol%%3BE");
    txd = string.gsub(txd, "%%5CE", "%%26;bsol%%3BE");

    m.log (1, "Lua: 5200.x.txr=’".. txr.. "’");
    if txr ~= txb then m.log (1, "Lua: 5200.x.txb='".. txb.. "'") end
    if txr ~= txd then m.log (1, "Lua: 5200.x.txd='".. txd.. "'") end
--]]


--[[
    -- Strategy 3: JDB-escaping
--]]


    -- Add JDB-escape patterns.
    txr = "@#@#".. txr.. "#@#@";
    txb = "0#0#".. txb.. "#@#@";
    txd = "0#0#".. txd.. "#0#@";


    -- Update with the results!
    m.setvar ("tx.HiTech_5200_reason",	  txr);
    m.setvar ("tx.HiTech_5200_body",	  txb);
    m.setvar ("tx.HiTech_5200_descScope", txd);

    --	Ensure the SecRule invoking this script considers it to "match": return "Lua-match";
    --	Ensure the SecRule invoking this script considers it to "not match"
    --	return nil;

end

