# John's Agile Lua - for WAF

This repository shows **Lua scripts** which are called by **ModSecurity (WAF)**
to help parse HTML bodies.
<br>


## Context

An old and insecure **backend web application** needed security fixes for ongoing use --<br>
but the software could not be updated in the required timeframe, for other business reasons.<br>
*SQL Injection* was a major concern.

I implemented a standard security paradigm of putting a **Web Application Firewall** (WAF)<br>
"in front of" the old software, implemented with **Apache reverse proxy**, **ModSecurity**,<br>
and the **OWASP Core Rule Set** (CRS)... and rigorous testing.

The incoming form-data and other structures were highly customized and quite complex.<br>
The user-facing web pages used an elaborate Javascript collection, hard to understand.

Due to limitations in the ModSecurty **rsub()** function and other constraints,<br>
I needed an "escape" mechanism to get **programmatic access** to the raw HTML structures<br>
during the ModSecurity parse chain.<br>

I achieved this with small **Lua scripts** which are well-supported by **ModSecurity**.<br>
The **Lua** language has a fast and robust implementation within **Apache**.<br>
**Lua** is simple, powerful, and well-suited as an embedded programming environment.<br>

[<img src="./zPix/ModSecurity-Overview.jpg" width="600" />](./zPix/ModSecurity-Overview.jpg)

<br>


## Sample Lua script invocation

This is one of several **ModSecurity** directives within the **Apache** configuration files.<br>
Lua script &nbsp; **5210-fix.lua** &nbsp; is invoked as part of the parse chain.

```
# - Simulate loop iterations by running the same sequence again several times (enough times).
#   Variables tx.HiTech_5210_old, tx.HiTech_5210_blurb are reused for Lua script convenience.

SecRule REQUEST_URI "@rx /web/BlickNote/portal/" \
    "id:'5211',  \
     chain,      \
     log,        \
     pass,       \
     phase:4,    \
     t:none,     \
    "

    SecRule STREAM_OUTPUT_BODY \
        "@rx (@#@#.*?#@#@)" \
        "capture,   \
         chain,     \
         setvar:tx.HiTech_5210_old=%{tx.0},   \
         setvar:tx.HiTech_5210_blurb=%{tx.0}, \
         t:none, \
        "

    SecRuleScript /link/mod_security/rules/Lua/5210-fix.lua \
        "chain, \
        "

    SecRule STREAM_OUTPUT_BODY \
        "@rsub s/\Q%{tx.HiTech_5210_old}\E/%{tx.HiTech_5210_blurb}/" \
        "t:none, \
        "
```
<br>


## Install Lua and Lua modules

- https://www.lua.org/docs.html
- https://luarocks.org/
- https://brew.sh/

```
mac$ date; whoami; uname -n
    Tue Oct 18 01:38:55 CEST 2022
    jdb
    jdbs-mac-mini-3.home

# - MacOS Catalina
mac$ system_profiler SPSoftwareDataType | grep -i version
      System Version: macOS 10.15.7 (19H2026)
      Kernel Version: Darwin 19.6.0

mac$ brew install lua luarocks
    ...

mac$ which lua; lua -v
    /usr/local/bin/lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio

mac$ which luarocks; luarocks --version
    /usr/local/bin/luarocks
    /usr/local/bin/luarocks 3.9.1
    LuaRocks main command-line interface

mac$ luarocks install html-entities
    Installing https://luarocks.org/html-entities-1.3.1-0.rockspec
    html-entities 1.3.1-0 depends on lua >= 5.1 (5.4-1 provided by VM)
    No existing manifest. Attempting to rebuild...
    html-entities 1.3.1-0 is now installed in /usr/local (license: MIT)

mac$ luarocks install base64
    Warning: falling back to curl - install luasec to get native HTTPS support
    Installing https://luarocks.org/base64-1.5-3.src.rock
    base64 1.5-3 is now installed in /usr/local (license: MIT/Public Domain)

mac$ luarocks install LuaSocket
    Installing https://luarocks.org/luasocket-3.1.0-1.src.rock
    luasocket 3.1.0-1 depends on lua >= 5.1 (5.4-1 provided by VM)
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/mime.c -o src/mime.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/compat.c -o src/compat.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc  -bundle -undefined dynamic_lookup -all_load -o mime/core.so src/mime.o src/compat.o
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/buffer.c -o src/buffer.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/compat.c -o src/compat.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/auxiliar.c -o src/auxiliar.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/options.c -o src/options.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/timeout.c -o src/timeout.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/io.c -o src/io.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/usocket.c -o src/usocket.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/serial.c -o src/serial.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc  -bundle -undefined dynamic_lookup -all_load -o socket/serial.so src/buffer.o src/compat.o src/auxiliar.o src/options.o src/timeout.o src/io.o src/usocket.o src/serial.o
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/luasocket.c -o src/luasocket.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/timeout.c -o src/timeout.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/buffer.c -o src/buffer.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/io.c -o src/io.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/auxiliar.c -o src/auxiliar.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/options.c -o src/options.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/inet.c -o src/inet.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/except.c -o src/except.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/select.c -o src/select.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/tcp.c -o src/tcp.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/udp.c -o src/udp.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/compat.c -o src/compat.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/usocket.c -o src/usocket.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc  -bundle -undefined dynamic_lookup -all_load -o socket/core.so src/luasocket.o src/timeout.o src/buffer.o src/io.o src/auxiliar.o src/options.o src/inet.o src/except.o src/select.o src/tcp.o src/udp.o src/compat.o src/usocket.o
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/buffer.c -o src/buffer.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/compat.c -o src/compat.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/auxiliar.c -o src/auxiliar.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/options.c -o src/options.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/timeout.c -o src/timeout.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/io.c -o src/io.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/usocket.c -o src/usocket.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/unix.c -o src/unix.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/unixdgram.c -o src/unixdgram.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc -O2 -fPIC -I/usr/local/opt/lua/include/lua5.4 -c src/unixstream.c -o src/unixstream.o -DLUASOCKET_DEBUG -DUNIX_HAS_SUN_LEN
    env MACOSX_DEPLOYMENT_TARGET=10.8 gcc  -bundle -undefined dynamic_lookup -all_load -o socket/unix.so src/buffer.o src/compat.o src/auxiliar.o src/options.o src/timeout.o src/io.o src/usocket.o src/unix.o src/unixdgram.o src/unixstream.o
    luasocket 3.1.0-1 is now installed in /usr/local (license: MIT)
```
<br>


## Confirm valid Lua syntax

```
mac$ for file in *.lua; do echo; echo == $file; lua -v $file; done

    == 5200-fix.lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio

    == 5210-fix.lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio

    == 5210-log-1.lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio

    == 5210-log-2.lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio

    == functions.lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio
```
<br>


## Confirm the function library

Here the **functions.lua** library is loaded and the functions are tested.<br>
Run these CLI tests from a **Bash shell** command line.

Results here can be compared with online tools, eg:

- https://www.freeformatter.com/html-escape.html

**Caution:**<br>
There may be more than one way to "escape" a given piece of text,<br>
but all results should "un-escape" back to the same starting string.

```
mac$ lua
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio
    > dofile("functions.lua");

    > url_1 = '/test/some string?foo=bar';
    > print ( urlEncode (url_1) );
    %2ftest%2fsome%20string%3ffoo%3dbar

    > url_2a = '%2ftest%2fsome%20string%3ffoo%3dbar';
    > print ( urlDecode (url_2a) )
    /test/some string?foo=bar

    > url_2b = '/test/some%20string?foo=bar';
    > print ( urlDecode (url_2b) )
    /test/some string?foo=bar


    > html_1 = 'hi <there> /guy, (from Uncle) "oh man"';
    > print ( htmlEncode (html_1) );
    &#104;&#105; &#60;&#116;&#104;&#101;&#114;&#101;&#62; &#47;&#103;&#117;&#121;&#44; &#40;&#102;&#114;&#111;&#109; &#85;&#110;&#99;&#108;&#101;&#41; &#34;&#111;&#104; &#109;&#97;&#110;&#34;

    > html_2a = 'hi&#32;&#60;there&#62;&#32;&#47;guy&#44;&#32;&#40;from&#32;Uncle&#41;&#32;&#34;oh&#32;man&#34;'
    > print ( htmlDecode (html_2a) );
    hi <there> /guy, (from Uncle) "oh man"

    > html_2b = '&#104;&#105; &#60;&#116;&#104;&#101;&#114;&#101;&#62; &#47;&#103;&#117;&#121;&#44; &#40;&#102;&#114;&#111;&#109; &#85;&#110;&#99;&#108;&#101;&#41; &#34;&#111;&#104; &#109;&#97;&#110;&#34;'
    > print ( htmlDecode (html_2b) );
    hi <there> /guy, (from Uncle) "oh man"


    > b64_1 = 'John Was Here';
    > print ( b64enc (b64_1) );
    Sm9obiBXYXMgSGVyZQ==

    > b64_2 = 'Sm9obiBXYXMgSGVyZQ==';
    > print ( b64dec (b64_2) );
    John Was Here

    > ^D
```
<br>


## Confirm the Lua scripts ?

These scripts are "subroutines" called from within **ModSecurity**,<br>
thus they need a full **WAF development environment** for testing.<br>

My **WAF** test platform is not described here, as it's a bigger project.<br>
Thus I don't show full testing of the **Lua scripts** here,<br>
only tests for the stand-alone function library as shown above.

```
mac$ ls 52*.lua | cat
    5200-fix.lua
    5210-fix.lua
    5210-log-1.lua
    5210-log-2.lua
```

### Diagram: WAF development environment

Surprisingly powerful **WAF development** is feasible these days on modern laptops and VMware.<br>
This diagram shows my dev/test environment for **OWASP WebGoat**.

- https://owasp.org/www-project-webgoat/


[<img src="./zPix/VMware_WebGoat.2019-11-09.png" width="800" />](./zPix/VMware_WebGoat.2019-11-09.png)

<br>


## Agile theme
This repository has useful working examples, quickly and iteratively adaptable to new environments.
<br/>


## License
Copyright &copy; 2019, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

