
-- version_5210_log_1_lua = '2.0.0'

-- log content, for diagnostics

function main()
    local body, r;

    -- local uid = m.getvar ("UNIQUE_ID", {"none"});
    -- m.log (1, "Lua: 5210-log-2.1, uid: ".. uid);

    body = m.getvar ("STREAM_OUTPUT_BODY", {"none"});
    r = string.len(body);
    -- m.log (1, "Lua: 5210-log-2.2, size=".. r);

    -- Diagnostic: save buffer to a file.
    local filePre = io.open ("/tmp/qjdb.log-2.html", "a");
    filePre:write ("\n<!-- Lua body -->\n");
    filePre:write (body);
    filePre:write ("\n<!-- done -->\n");
    filePre:close();

    -- Ensure the SecRule invoking this script considers it to "match":
    return "Lua-match";
end

