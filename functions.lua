
--[[
    -- Earlier implementation:
    solaris$ lua -v
    Lua 5.1.4 Copyright (C) 1994-2008 Lua.org, PUC-Rio

    -- This implementation:
    mac$ lua -v
    Lua 5.4.4  Copyright (C) 1994-2022 Lua.org, PUC-Rio
--]]


-- version_functions_lua = '2.0.0'


-- urlDecode & urlDecode
--
-- ModSecurity urlDecode isn't adequate, if we have to do it twice.
-- blurb = m.getvar ("tx.HiTech_5210_blurb", ("urlDecode"});
--
-- https://w3.impa.br/%7Ediego/software/luasocket/url.html#unescape

-- > url_1 = '/test/some string?foo=bar';
-- > print ( urlEncode (url_1) );
-- %2ftest%2fsome%20string%3ffoo%3dbar

function urlEncode (str)
    local url = require("socket.url")
    local escaped = url.escape(str)

    return escaped
end


-- > url_2a = '%2ftest%2fsome%20string%3ffoo%3dbar';
-- > print ( urlDecode (url_2a) )
-- /test/some string?foo=bar
--
-- > url_2b = '/test/some%20string?foo=bar';
-- > print ( urlDecode (url_2b) )
-- /test/some string?foo=bar

function urlDecode (str)
    local url = require("socket.url")
    local unescaped = url.unescape(str)

    return unescaped
end



-- HTML encode and decode
--
-- So the output is Content-Type: text/html
-- https://github.com/TiagoDanin/htmlEntities-for-lua

-- > html_1 = 'hi <there> /guy, (from Uncle) "oh man"';
-- > print ( htmlEncode (html_1) );
-- &#104;&#105; &#60;&#116;&#104;&#101;&#114;&#101;&#62; &#47;&#103;&#117;&#121;&#44; &#40;&#102;&#114;&#111;&#109; &#85;&#110;&#99;&#108;&#101;&#41; &#34;&#111;&#104; &#109;&#97;&#110;&#34;
--
-- FYI: previous implementations were less rigorous, leaving "ordinary text" unencoded.
-- hi&#32;&#60;there&#62;&#32;&#47;guy&#44;&#32;&#40;from&#32;Uncle&#41;&#32;&#34;oh&#32;man&#34;

function htmlEncode (str)
    local htmlEntities = require('htmlEntities')
    local encoded = htmlEntities.encode(str)

    return encoded
end


-- > html_2a = 'hi&#32;&#60;there&#62;&#32;&#47;guy&#44;&#32;&#40;from&#32;Uncle&#41;&#32;&#34;oh&#32;man&#34;'
-- > print ( htmlDecode (html_2a) );
-- hi <there> /guy, (from Uncle) "oh man"
--
-- > html_2b = '&#104;&#105; &#60;&#116;&#104;&#101;&#114;&#101;&#62; &#47;&#103;&#117;&#121;&#44; &#40;&#102;&#114;&#111;&#109; &#85;&#110;&#99;&#108;&#101;&#41; &#34;&#111;&#104; &#109;&#97;&#110;&#34;'
-- > print ( htmlDecode (html_2b) );
-- hi <there> /guy, (from Uncle) "oh man"

function htmlDecode (str)
    local htmlEntities = require('htmlEntities')
    local decoded = htmlEntities.decode(str)

    return decoded
end



-- Base64 encode and decode
-- https://luarocks.org/modules/iskolbin/base64
-- https://github.com/iskolbin/lbase64

-- > b64_1 = 'John Was Here';
-- > print ( b64enc (b64_1) );
-- Sm9obiBXYXMgSGVyZQ==

function b64enc (str)
    local base64 = require('base64')
    local encoded = base64.encode( str )

    return encoded
end


-- > b64_2 = 'Sm9obiBXYXMgSGVyZQ==';
-- > print ( b64dec (b64_2) );
-- John Was Here

function b64dec (b64str)
    local base64 = require('base64')
    local decoded = base64.decode( b64str )

    return decoded
end


