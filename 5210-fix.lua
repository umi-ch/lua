
-- version_5210_fix_lua = '2.0.0'

-- process BlickNote data within output response body, before sent on to user's browser

function main()
    local blurb;

    local topDir = '/app';
    package.path = package.path.. topDir.. '/link/mod_security/rules/Lua/?.lua'
    require ("functions");

    blurb = m.getvar ("tx.HiTech_5210_blurb", {"none"});


    -- decode forgivingly: {"base64decodeExt"}
    --	m.log (1, "Lua: 5210-fix.1, blurb: ".. blurb);

    --	Data from http input requests arrives urlEncoded:
    --      Content-Type: application/x-www-form-encoded
    --	Data sent into http output requests should be htmlEncoded:	Content-Type:	text/html
    --
    --	Strategy 1: the BlickNote is:
    --      -	b64Encoded within http input request, before submitted to WUI
    --      -	b64Decoded within output response body, before sent on to user's browser
    --
    --	Strategy 2: the BlickNote is:
    --      -	htmlEncoded within http input request, before submitted to WUI
    --      -   htmlDecoded within output response body, before sent on to user's browser
    --
    --	Strategy 3: the BlickNote is
    --      -	JDB-escaped within http input request, before submitted to WUI
    --      -	JDB-unescaped within output response body, before sent on to user's browser
    --
    --	- Strategy 1 also requires strategy 3, else the text is no casually readable.
    --	- Strategy 2 might not require strategy 3, though it's more deterministic to do so.


--[[
    --	Strategy 1: b64Decode
--]]

    --	Convert spaces back into '+' plus signs. (b64 strategy)
    --	That is, undo a spurious urlEncoding.
    --
    blurb = string.gsub(blurb, ' ',	'+');

    --	Do the JDB-unescaping here:
    blurb = string.gsub(blurb, "A@#@#", "");        -- "__bozo-"
    blurb = string.gsub(blurb, "#@#@$", "");        -- "-bozo__"

    --	Base-64 decoding:
    blurb = b64dec(blurb);                          -- decode forgivingly.



--[[
    --	Strategy 2: htmlDecode
    --	No explicit action needed, except for quick-fix below,
    --	and avoid re-htmlEncoding it at the end.
    --	Quirk-fix: convert '+ plus signs into spaces (urlEncode strategy)
    --	That is, perform a "spurious" urlEncoding.
    --
    blurb = string.gsub(blurb, '\+', ' ');
    --	m.log (1, "Lua: 5210-fix.2, blurb:	blurb);
--]]


--[[
    -- Strategy 2: htmlDecode ?
    -- Do the JDB-unescaping here:
    blurb = string.gsub(blurb, "A@#@#", "");	--	"__bozo-"
    blurb = string.gsub(blurb, "#@#@$", "");	--	"-bozo__"
    -- m.log (1, "Lua: 5210-fix.J, blurb: ".. blurb);
--]]


--[[
    --	Strategy 2: htmlDecode ?
    == Suppress this step, when using htmlDecode strategy 2 ==
--]]

    --	Data sent into http output requests should be htmlEncoded:
    --      Content-Type: text/html
    blurb = htmlEncode (blurb);

    -- Update with the results!
    m.setvar ("tx.HiTech_5210_blurb", blurb);

    --	Ensure the SecRule invoking this script considers it as matching:
    return "Lua-match";

    --	Ensure the SecRule invoking this script considers it as not-matching:
    --	return nil;
end

